import './App.css';
import {Route, Switch} from "react-router-dom";
import React, {useEffect, useState} from "react";
import SiteLoader from "./components/Loader/SiteLoader";
import Home from "./pages/Home/Home";
import Sidebar from "./components/Sidebar/Sidebar";
import Products from "./pages/Products/Products";
import Sale from "./pages/Sale/Sale";
import Cashbox from "./pages/Cashbox/Cashbox";
import Branch from "./pages/Branch/Branch";
import Currency from "./pages/Currency/Currency";

function App() {

    const [loading, setLoading] = useState(true);

    const [showSidebar, setShowSidebar] = useState(true);

    useEffect(()=>{
        setLoading(false);
    },[]);

    if (loading) {
        return (
            <SiteLoader/>
        )
    }

    return (
        <div className="content">
            <Sidebar showSidebar={showSidebar}  setShowSidebar={setShowSidebar}/>
            <div className={`main ${!showSidebar ? "show-main": ''}`}>

                <Switch>

                    <Route exact path="/home" component={Home}/>
                    <Route path="/products" component={Products}/>
                    <Route path="/sale" component={Sale}/>
                    <Route exact path="/cashbox" component={Cashbox}/>
                    <Route exact path="/branch" component={Branch}/>
                    <Route exact path="/currency" component={Currency}/>

                </Switch>

            </div>
        </div>
    );
}

export default App;
