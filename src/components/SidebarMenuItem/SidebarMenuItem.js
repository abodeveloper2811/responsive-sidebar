import React, {useState} from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import SingleMenuItem from "../SingleMenuItem/singleMenuItem";
import {Link, NavLink} from "react-router-dom";

const SidebarMenuItem = ({item, showSidebar, setShowSidebar}) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () =>{
        setIsOpen(!isOpen);
        setShowSidebar(true);
    };

    return (
        <div>

            <div className={`menu-item-box ${isOpen ? "sub-menu-open" : ""}`} onClick={toggle}>

                <div className="d-flex align-items-center">
                    <div className="icon-box">
                        <div className={`${item.icon}`}></div>
                    </div>

                    {
                        showSidebar && (
                            <span className="title">{item.title}</span>
                        )
                    }
                </div>

                {
                    showSidebar && (
                        <div className={`icon icon-arrow ${isOpen ? "icon-arrow-rotate" : ""}`}>

                        </div>
                    )
                }

            </div>


            <Collapse isOpen={isOpen}>
                {
                    showSidebar && (
                        <ul className="sub-menu">
                            {
                                item.subMenuItems.map((menu, index)=>(
                                    <li className="sub-menu-item">
                                        {
                                            menu.open ?

                                                <SingleMenuItem singleItem={menu}/> :

                                                <NavLink className="sub-menu-item-link" to={`${menu.pathName}`}>
                                                    {menu.title}
                                                </NavLink>
                                        }
                                    </li>
                                ))
                            }
                        </ul>
                    )
                }
            </Collapse>

        </div>
    );
};

export default SidebarMenuItem;