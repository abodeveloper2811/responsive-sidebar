import React, {useState} from 'react';
import {Button, Collapse} from "reactstrap";
import {Link, NavLink} from "react-router-dom";
import Arrow from '../../images/right-arrov.png'

const SingleMenuItem = ({singleItem}) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <div className={`sub-menu-item-link ${isOpen ? "sub-menu-item-link-active" : ""}`} color="success" onClick={toggle}>

                <img src={Arrow} alt=""  className={`right-arrow ${isOpen ? "arrow-rotate" : ""}`}/>

                <span className="ms-2">{singleItem.title}</span>

            </div>
            <Collapse isOpen={isOpen}>
                <ul className="single-sub-menu">
                    {
                        singleItem.openMenu.map((menu, index)=>(
                            <li className="single-sub-menu-item">
                                <NavLink activeClassName="single-menu-item-link-active" to={`${menu.pathName}`}>
                                    {menu.title}
                                </NavLink>
                            </li>
                        ))
                    }
                </ul>
            </Collapse>



        </div>
    );
};

export default SingleMenuItem;