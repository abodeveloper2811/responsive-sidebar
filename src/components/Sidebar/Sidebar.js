import React from 'react';
import Avatar from '../../images/Avatar.jpg'
import "./Sidebar.css"
import {Link, NavLink} from "react-router-dom";
import SidebarMenuItem from "../SidebarMenuItem/SidebarMenuItem";
import "../../mask_icon.css";
import Arrow from '../../images/left-arrow.png';
import Nuqta from "../../images/nuqta.png"

const Sidebar = ({showSidebar, setShowSidebar}) => {

    const sidebarMenu = [
        {
            icon: "icon icon-home",
            title: "Bosh sahifa",
            pathName: '/home',
            subMenu: false,
        },
        {
            icon: "icon icon-mahsulot",
            title: "Mahsulotlar",
            subMenu: true,
            subMenuItems: [
                {
                    title: "Yaratish",
                    open: true,
                    openMenu: [
                        {
                            title: "1-sahifa",
                            pathName: '/products/create/sahifa1'
                        },
                        {
                            title: "2-sahifa",
                            pathName: '/products/create/sahifa2'
                        },
                        {
                            title: "3-sahifa",
                            pathName: '/products/create/sahifa3'
                        }
                    ]
                },
                {
                    title: "Mahsulotlar hisoboti",
                    pathName: "report",
                    open: false,
                },
                {
                    title: "Qabul qilish",
                    pathName: 'receive',
                    open: false,
                },
                {
                    title: "Inventarizatsiya",
                    open: true,
                    openMenu: [
                        {
                            title: "1-sahifa",
                            pathName: '/products/invertization/sahifa1'
                        },
                        {
                            title: "2-sahifa",
                            pathName: '/products/invertization/sahifa2'
                        },
                        {
                            title: "3-sahifa",
                            pathName: '/products/invertization/sahifa3'
                        }
                    ]
                }
            ]
        },
        {
            icon: "icon icon-sotuv",
            title: "Sotuv",
            subMenu: true,
            subMenuItems: [
                {
                    title: "Sotuvlar hisoboti",
                    pathName: "report",
                    open: false,
                },
                {
                    title: "Qabul qilish",
                    pathName: "receive",
                    open: false,
                }
            ]
        },
        {
            icon: "icon icon-kassa",
            title: "Kassa",
            pathName: 'cashbox',
            subMenu: false,
        },
        {
            icon: "icon icon-filial",
            title: "Filial",
            pathName: '/branch',
            subMenu: false,
        },
        {
            icon: "icon icon-valyuta",
            title: "Valyuta kursi",
            pathName: '/currency',
            subMenu: false,
        },
    ];

    return (
        <div  className={`sidebar ${!showSidebar ? "hide-sidebar": ''}`}>

            <div className="sidebar-top">
                <div className="avatar-box">
                    <div className="img-box">
                        <img src={Avatar} alt="Avatar"/>
                    </div>

                    <>
                        {
                            showSidebar && (
                                <div className="title">
                                    <div className="name">Abbos Rakhmonov</div>
                                    <div className="position">Director</div>
                                </div>
                            )
                        }

                    </>

                </div>

                <>
                    {
                        showSidebar && (
                            <div className="profile-box">
                                <img src={Nuqta} alt=""/>
                            </div>
                        )
                    }

                </>

                <div className={`left-arrow-box`}  onClick={()=>setShowSidebar(!showSidebar)}>
                    <img className={`${!showSidebar ? "right-arrow": ''}`} src={Arrow} alt=""/>
                </div>

            </div>

            <div className="menu-box">
                <ul className="sidebar-menu">
                    {
                        sidebarMenu.map((item, index)=>(
                            <li key={index} className="menu-item">
                                {
                                    item.subMenu ?

                                        <SidebarMenuItem item={item} showSidebar={showSidebar} setShowSidebar={setShowSidebar}/> :

                                        <NavLink activeClassName="active-item" className="menu-item-box" to={`${item.pathName}`}>

                                            <div className="d-flex align-items-center">
                                                <div className="icon-box">
                                                    <div className={`${item.icon}`}></div>
                                                </div>

                                                {
                                                    showSidebar && (
                                                        <span className="title">{item.title}</span>
                                                    )
                                                }

                                            </div>

                                        </NavLink>
                                }
                            </li>
                        ))
                    }

                </ul>
            </div>

        </div>
    );
};

export default Sidebar;